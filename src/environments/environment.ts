// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    url: 'http://localhost/angular/slim/',
    firebase:{
    apiKey: "AIzaSyDeHgQVXloPfzCJs4Rkw-rvPaIraSjPwgw",
    authDomain: "angular-77b61.firebaseapp.com",
    databaseURL: "https://angular-77b61.firebaseio.com",
    projectId: "angular-77b61",
    storageBucket: "angular-77b61.appspot.com",
    messagingSenderId: "255029561642"
  }
};

import { Component, OnInit } from '@angular/core';
import { ProductsService } from './../products.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {
  products;
  productsKeys;

  /*updateProduct(id){
    this.service.getProduct(id).subscribe(response=>{
        this.products = response.json();      
    })
  }*/

  /*updateProduct(name){
    this.service.getSearch(name).subscribe(response=>{
        this.products = response.json();     
        this.productsKeys = Object.keys(this.products); 
    })
  }*/

  constructor(private service:ProductsService, private route: ActivatedRoute) {
      this.service = service;
   }
  product;
  invalid = false;
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('name');
      console.log(id);
      this.service.getSearch(id).subscribe(response=>{
        this.products = response.json();
        this.productsKeys = Object.keys(this.products); 
        console.log(this.productsKeys);
        if(this.productsKeys != null)
          this.invalid = true;
        else
          this.invalid = false;
        console.log(this.products);
      })
    })
  }

}
